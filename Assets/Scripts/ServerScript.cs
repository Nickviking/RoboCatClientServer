﻿using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

public class ServerClient
{
    public int connectionId;
    public string playerName;
    public string playerIP;
    public int port;
    public Vector2 position;
    public float angle;

}

public class ServerScript : MonoBehaviour
{
    private const int MAX_CONNECTION = 100;

    private int port = 8888;

    private int hostId;
    private int webHostId;

    private int reliableChannel;
    private int unreliableChannel;

    private bool isStarted = false;
    private byte error;

    private List<ServerClient> clients = new List<ServerClient>();

    private float lastMovimentUpdate;
    private float movimentUpdateRate = 0.2f;

    public InputField portInputField;
    public Text status;

    private void Start()
    {
        status.text = "Server closed";
    }

    public void StartServer()
    {
        port = int.Parse(portInputField.text);
        NetworkTransport.Init();
        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);
        unreliableChannel = cc.AddChannel(QosType.Unreliable);

        HostTopology topo = new HostTopology(cc, MAX_CONNECTION);

        hostId = NetworkTransport.AddHost(topo, port, null);
        webHostId = NetworkTransport.AddWebsocketHost(topo, port, null);

        isStarted = true;
        status.text = "Server Open on Port: " + port;
    }
    private void Update()
    {
        if (!isStarted)
            return;
        #region reciveDataFromClients
        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {
            #region connectionEvent
            case NetworkEventType.ConnectEvent:
                Debug.Log("Player " + connectionId + " has connected");
                OnConnection(connectionId);

                break;
            #endregion
            #region dataEvent
            case NetworkEventType.DataEvent:
                string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                Debug.Log("Receiving from " + connectionId + " : " + msg);
                string[] splitData = msg.Split('|');

                switch (splitData[0])
                {
                    case "NAMEIS":
                        OnNameIs(connectionId, splitData[1]);
                        break;
                    case "MYPOSITION":
                        OnMyPosition(connectionId, splitData);
                        break;
                    case "MYSHOOT":
                        OnMyShoot(connectionId);
                        break;
                    default:
                        Debug.Log("Invalid Message: " + msg);
                        break;
                }
                break;
            #endregion
            #region disconnectEvent
            case NetworkEventType.DisconnectEvent: //4
                OnDisconnection(connectionId);
                Debug.Log("Player " + connectionId + " has disconnected");
                break;
                #endregion
        }
        #endregion
    }

    private void OnNameIs(int connId, string playerName)
    {
        //Link the name to connection ID
        clients.Find(x => x.connectionId == connId).playerName = playerName;

        //Tell every body that a new player has connected

        Send("CNN|" + playerName + "|" + connId, reliableChannel, clients);
    }

    private void OnConnection(int connId)
    {
        // Add him to a list
        ServerClient c = new ServerClient();
        c.connectionId = connId;
        c.playerName = "TEMP";
        clients.Add(c);

        //When the player joins the server, tell him his ID
        //Request his name and send the name of all the other players

        string msg = "ASKNAME|" + connId + "|";
        foreach (ServerClient client in clients)
        {
            msg += client.playerName + "%" + client.connectionId + "|";
        }
        msg = msg.Trim('|');

        //ASKNAME|3|NICK%1|STEVE%2|TEMPO%3

        Send(msg, reliableChannel, connId);

    }

    private void OnDisconnection(int connId)
    {
        //Remove this player from out client list
        clients.Remove(clients.Find(x => x.connectionId == connId));

        //Tell everyone that somebody else has disconnected
        Send("DC|" + connId, reliableChannel, clients);
    }

    private void OnMyPosition(int connId, string[] splitData)
    {
        string[] stringPosition = splitData[1].Split('%');
        Vector2 position = Vector2.zero;
        position.x = float.Parse(stringPosition[0]);
        position.y = float.Parse(stringPosition[1]);
        float angle = float.Parse(stringPosition[2]);

        clients.Find(c => c.connectionId == connId).position = position;
        clients.Find(c => c.connectionId == connId).angle = angle;

        string m = "UPDATEPOSITION|";

        m += connId.ToString() + '%' + position.x.ToString() + '%' + position.y.ToString() + '%' + angle.ToString();
        Send(m, unreliableChannel, clients, connId);
    }

    private void OnMyShoot(int connId)
    {
        string message = "SHOT|" + connId.ToString();
        Send(message, reliableChannel, clients, connId);
    }

    private void Send(string message, int channelId, int connId)
    {
        List<ServerClient> c = new List<ServerClient>();
        c.Add(clients.Find(x => x.connectionId == connId));
        Send(message, channelId, c);
    }

    private void Send(string message, int channelId, List<ServerClient> c, int connId)
    {
        List<ServerClient> clientlist = new List<ServerClient>();
        foreach (ServerClient item in clients)
        {
            clientlist.Add(item);
        }
        clientlist.Remove(clientlist.Find(x => x.connectionId == connId));
        Send(message, channelId, clientlist);
    }

    private void Send(string message, int channelId, List<ServerClient> c)
    {
        Debug.Log("Sending: " + message);
        byte[] msg = Encoding.Unicode.GetBytes(message);
        foreach (ServerClient client in c)
        {
            NetworkTransport.Send(hostId, client.connectionId, channelId, msg, message.Length * sizeof(char), out error);
        }
    }

}