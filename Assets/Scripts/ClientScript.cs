﻿using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;
using System.Collections.Generic;

public class Player
{
    public string playerName;
    public GameObject avatar;
    public int connectionId;
    public Rigidbody2D playerRigidBody;
}

public class ClientScript : NetworkBehaviour
{
    private const int MAX_CONNECTION = 100;

    private int port = 8888;

    private int hostId;
    private int webHostId;

    private int reliableChannel;
    private int unreliableChannel;

    private int ourClientId;
    private int connectionId;

    public float connectionTime;
    private bool isConnected = false;
    private bool isStarted = false;

    private byte error;

    private string playerName;

    public GameObject playerPrefab;
    public Dictionary<int, Player> players = new Dictionary<int, Player>();

    public InputField ipInputField;
    public InputField portInputField;
    public GameObject canvas;

    public void Connect()
    {

        NetworkTransport.Init();
        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);
        unreliableChannel = cc.AddChannel(QosType.Unreliable);

        HostTopology topo = new HostTopology(cc, MAX_CONNECTION);



        hostId = NetworkTransport.AddHost(topo, 0);

        connectionId = NetworkTransport.Connect(hostId, ipInputField.text, int.Parse(portInputField.text), 0, out error);

        connectionTime = Time.time;
        isConnected = true;
    }

    private void Update()
    {
        if (!isConnected)
            return;

        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {
            case NetworkEventType.DataEvent:
                string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                Debug.Log("Receiving: " + msg);
                string[] splitData = msg.Split('|');

                switch (splitData[0])
                {
                    case "ASKNAME":
                        OnAskName(splitData);
                        break;
                    case "CNN":
                        SpawnPlayer(splitData[1], int.Parse(splitData[2]));
                        break;
                    case "DC":
                        PlayerDisconnect(int.Parse(splitData[1]));
                        break;
                    case "UPDATEPOSITION":
                        OnUpdatePosition(splitData);
                        break;
                    case "SHOT":
                        OnShot(int.Parse(splitData[1]));
                        break;
                    default:
                        Debug.Log("Invalid Message: " + msg);
                        break;
                }
                break;
        }

    }

    private void OnAskName(string[] data)
    {
        //Set this client's ID
        ourClientId = int.Parse(data[1]);

        //Send our name to the server
        Send("NAMEIS|" + playerName, reliableChannel);


        //Create all the other players

        for (int i = 2; i < data.Length - 1; i++)
        {
            string[] d = data[i].Split('%');
            SpawnPlayer(d[0], int.Parse(d[1]));
        }
    }

    public void SendMyPosition(float x, float y, float angle)
    {
        string m = "MYPOSITION|" + x.ToString() + '%' + y.ToString() + '%' + angle.ToString();
        Send(m, unreliableChannel);
    }

    public void Shoot()
    {
        string message = "MYSHOOT";
        Send(message, reliableChannel);
    }

    private void OnUpdatePosition(string[] data)
    {
        if (!isStarted)
        {
            return;
        }

        //Update everyone else
        for (int i = 1; i < data.Length; i++)
        {
            string[] d = data[i].Split('%');
            //Prevent the server to update us
            if (ourClientId != int.Parse(d[0]))
            {
                Vector2 position = Vector2.zero;
                position.x = float.Parse(d[1]);
                position.y = float.Parse(d[2]);
                players[int.Parse(d[0])].avatar.transform.position = position;
                players[int.Parse(d[0])].avatar.transform.rotation = Quaternion.Euler(0, 0, float.Parse(d[3]));
            }
        }

        //Vector2 myPosition = players[ourClientId].avatar.transform.position;
        //string m = "MYPOSITION|" + myPosition.x.ToString() + '|' + myPosition.y.ToString() + '|' + players[ourClientId].avatar.transform.eulerAngles.z;
        //Send(m, unreliableChannel);
        //Send our own position
    }

    private void OnShot(int connId)
    {
        if (!isStarted)
        {
            return;
        }
        players[connId].avatar.GetComponent<PlayerGun>().Shoot();
    }

    private void SpawnPlayer(string name, int id)
    {
        GameObject go = Instantiate(playerPrefab) as GameObject;

        //is this ours?
        Player p = new Player();
        p.playerRigidBody = go.GetComponent<Rigidbody2D>();
        if (id == ourClientId)
        {
            //add mobility
            go.AddComponent<PlayerControllerGato>();
            isStarted = true;
            canvas.SetActive(false);
            p.playerRigidBody.bodyType = RigidbodyType2D.Dynamic;
            go.GetComponent<NetworkId>().id = id;
            go.GetComponent<NetworkId>().isEnemy = false;
        }

        p.avatar = go;
        p.playerName = name;
        p.connectionId = id;
        // p.avatar.GetComponentInChildren<TextMesh>().text = id + " " + name;
        players.Add(id, p);

        SendMyPosition(players[connectionId].avatar.transform.position.x, players[connectionId].avatar.transform.position.y, players[connectionId].avatar.transform.eulerAngles.z);
    }

    public void Disconnect()
    {
        NetworkTransport.Disconnect(hostId, connectionId, out error);
    }

    private void PlayerDisconnect(int connId)
    {
        Destroy(players[connId].avatar);
        players.Remove(connId);

    }

    private void Send(string message, int channelId)
    {
        Debug.Log("Sending: " + message);
        byte[] msg = Encoding.Unicode.GetBytes(message);
        NetworkTransport.Send(hostId, connectionId, channelId, msg, message.Length * sizeof(char), out error);
    }

}