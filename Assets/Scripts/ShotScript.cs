﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotScript : MonoBehaviour
{
    private GameObject parent;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (parent == null)
        {
            parent = collision.gameObject;
        }
        if (collision.gameObject != parent && collision.transform.tag.Equals("Player"))
        {
            if (!collision.gameObject.GetComponent<NetworkId>().isEnemy)
            {
                collision.gameObject.GetComponent<PlayerControllerGato>().Shooted();
            }
            Destroy(gameObject);
        }
    }
}
