﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SetupLocalPlayer : NetworkBehaviour
{

    [SyncVar]
    public string name = "Player";

    [SyncVar]
    public Color playerColor = Color.white;

    private void OnGUI()
    {
        if (isLocalPlayer)
        {
            name = GUI.TextField(new Rect(25, Screen.height - 40, 100, 30), name);
            if (GUI.Button(new Rect(130, Screen.height - 40, 80, 30), "Change"))
            {
                CmdChangeName(name);
            }
        }

    }


    // Use this for initialization
    void Start()
    {
        if (isLocalPlayer)
        {
            GetComponent<PlayerControllerGato>().enabled = true;
            transform.GetChild((transform.childCount - 1)).gameObject.SetActive(true);

            Renderer[] rends = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in rends)
            {
                r.material.color = playerColor;
            }

        }

        this.transform.position = new Vector3(Random.Range(-20, 20), 0, Random.Range(-20, 20));
    }

    private void Update()
    {
        this.GetComponentInChildren<TextMesh>().text = name;
    }
    [Command]
    public void CmdChangeName(string newName)
    {
        name = newName;
    }



}
